'''
Created on 11.06.2019

@author: mamo06
'''
import cherrypy
import os
import subprocess

#TODO: global locking auf threads
#TODO: timeout for acquire
#TODO: switch off multi threading, reduce threwad pool to 1
class DeployService(object):
    
    
    def __init__ (self):
        self.to_deploy = False
        

    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def index(self):
        message = ''
        #cherrypy.session.acquire_lock()
        try:
            candidate = cherrypy.request.headers['X-Gitlab-Token']
            try:
                secret = os.environ.get('X-Gitlab-Token')
                if secret == candidate:
                    try:
                        #myproc = subprocess.call("./depl.sh")#, timeout=600)
                        #print(cherrypy.session)
                        #os.system('./depl.sh')
                        #os.getenv 
                        message = 'Everything works fine.'
                        self.to_deploy = True
                    except:
                        message = 'Shell script failed. Please run on Linux or test the deployment.'
                else:
                    message = 'Authentication failed. Incorrect or unspecified X-Gitlab-Token.'
                    return message
            except:
                message = 'No system variable "X-Gitlab-Token" available.'
                return message
        except:
            message = 'No header parameter "X-Gitlab-Token" available.'
            return message
                                           
        #cherrypy.session.release_lock()
        return message
    
    
    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def poll_for_update(self):
        
        if self.to_deploy:
            self.to_deploy = False
            return {'to_deploy': True}
        else:
            return {'to_deploy': False}
        


if __name__ == '__main__':
    
    # locking is handle automatically by Cherrypy.
    # However, handing over the 'explicit' value for locking gives us freedom to manually manipulate settings without loosing the secure defaults
    config = {'server.socket_host': '0.0.0.0', 'tools.sessions.on': True, 'server.thread_pool': 1}# 'tools.sessions.locking': 'explicit'}
    cherrypy.config.update(config)
    cherrypy.quickstart(DeployService())
    
