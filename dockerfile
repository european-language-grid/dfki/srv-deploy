from python:3.6.4-slim-jessie

RUN pip install CherryPy
COPY deploy_webservice.py .
COPY depl.sh .
RUN chmod +x depl.sh
EXPOSE 8080
ENTRYPOINT ["python", "deploy_webservice.py"]